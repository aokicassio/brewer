##README##
## About deploying app to Heroku ##

**1) Install Heroku and Git on your machine **

**2) Upload project files, creating local repo with Git**

Example:

```
#!shell

git init
git remote add heroku https://git.heroku.com/dinobrewer.git
git remote -v
```

**3) Configure Heroku app variables**

```
#!shell

heroku config:set SENDGRID_PASSWORD=password
heroku config:set AWS_ACCESS_KEY_ID=accesskeyhere
heroku config:set AWS_SECRET_ACCESS_KEY=secretaccesskeyhere
```
**4) Create JawsDB app in Heroku and migrate with Flyway all database files to the created instance (performed with Eclipse or similar)**

**5) Check .gitignore**

```
#!shell

git status
git add .
git commit -m "Deployable version"
git push heroku master
```
This might be enough to start working with a production app!